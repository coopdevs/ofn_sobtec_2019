title: Open Food Network
layout: true

---

class: header, impact

# {{title}}
## .alt-light[Sobtec 2019]

---

Pau Pérez

.em.thin.small[Membre de Coopdevs]<br>
.em.thin.small[al Core Team d'Open Food Network]<br>
.em.thin.small[@prez_pau]

---

class: header

## Visió

---

Millorar els sistemes alimentaris

![](img/better_food_systems.png)

---

Potenciar circuits curts de distribució

![](img/supply_chain.png)

---

background-image: url(img/ofn_map.png)
background-size: contain

Xarxa internacional de projectes locals

---

Software & coneixement<br>
com un bé comú

---

Mercat distribuit per a<br>
organitzacions alimentàries

---

background-image: url(img/frontoffice.png)

---

background-image: url(img/backoffice.png)

---

class: header

## Com treballem

---

Pot únic de diners

![](img/money.png)

---

background-image: url(img/global_team.png)

Equip global

---

background-image: url(img/gathering.png)

.light.text-shadow[Trobades internacionals anuals]


---

Voting process

---

background-image: url(img/voting_process.png)

---

The Pipeline

---

background-image: url(img/pipeline.png)

---

background-image: url(img/epic.png)

---

background-image: url(img/blocked.png)

---

Code Review & Merge

---

background-image: url(img/merge.png)
background-size: 70%

---

Testing

---

background-image: url(img/testing.png)
background-size: 65%

---

Release management

---

background-image: url(img/release.png)
background-size: 80%

---

class: header

## Comunicació

---

Espai per debats

---

background-image: url(img/discourse.png)

---

Comunicació diària

---

background-image: url(img/slack.png)

---

Planificació setmanal

---

background-image: url(img/thisweek.png)
background-size: 90%

---

OFN Delivery Train catchup

![](img/phone.png)

---

Agenda

.em.thin.x-small[Review of the process (continuous improvement)]<br>
.em.thin.x-small[Status of current roadmap priorities]<br>
.em.thin.x-small[Updates on how major things are going]<br>
.em.thin.x-small[Understand if there's room for anything else in the pipe]<br>
.em.thin.x-small[Whatever else the group wants to cover]

---

class: header

## Flux de contribució

---

Issues *deluxe*

---

background-image: url(img/good_first_issue.png)

---

acollida i benvinguda

![](img/heart.png)

---

background-image: url(img/invitation.png)
background-size: 80%

---

class: header

## Katuma

---

background-image: url(img/katuma_map.png)

.light.text-shadow[Instància ibèrica d'OFN]

---

background-image: url(img/commons_levels.jpg)

3 nivells de comú

---

Cooperativa de serveis:<br>
socis de consum i producció

---

Viabilitat econòmica<br>
sense ànim de lucre

---

class: header

Enllaços

.thin.small[gitlab.com/coopdevs/ofn_sobtec_2019]
.thin.small[github.com/openfoodfoundation/openfoodnetwork/]

---

class: header

## Preguntes

